﻿using System;
using IndexMaker.Model;
using System.Collections.Generic;
using System.IO;

namespace IndexMaker.Design
{
    public class DesignFileInfoService : IFileInfoService
    {
        public void SaveFileInfo(string filename, IEnumerable<string> fileInfos, Action<Exception> callback)
        {
            callback(null);
        }

        public void GetFileInfo(string path, string searchPattern, bool includeSubfolders, Action<System.Collections.Generic.IEnumerable<string>, Exception> callback)
        {
            int length = 50;
            var files = new List<string>(length);
            for (int i = 0; i < length; i++)
            {
                files.Add(Path.GetRandomFileName());
            }

            callback(files, null);
        }
    }
}