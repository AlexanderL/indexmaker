﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using IndexMaker.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Windows.Input;

namespace IndexMaker.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IFileInfoService fileInfoService)
        {
            _fileInfoService = fileInfoService;
            ViewTitle = "Index maker for Windows Explorer";
        }

        private readonly IFileInfoService _fileInfoService;
        private const string DefaultSearchPattern = "*.*";

        #region Properties

        private ObservableCollection<string> _files;
        public ObservableCollection<string> Files
        {
            get
            {
                return _files;
            }

            set
            {
                _files = value;
                RaisePropertyChanged(() => Files);
                RaisePropertyChanged(() => Info);
            }
        }

        private string _path;

        public string Path
        {
            get
            {
                return _path;
            }

            set
            {
                if (_path == value)
                {
                    return;
                }

                _path = value;
                RaisePropertyChanged(() => Path);
                GetFiles();
            }
        }

        private string _searchPattern = DefaultSearchPattern;

        public string SearchPattern
        {
            get
            {
                return _searchPattern;
            }

            set
            {
                if (_searchPattern == value)
                {
                    return;
                }

                _searchPattern = value;
                RaisePropertyChanged(() => SearchPattern);
                GetFiles();
            }
        }

        private bool _includeSubfolders = false;

        public bool IncludeSubfolders
        {
            get
            {
                return _includeSubfolders;
            }

            set
            {
                if (_includeSubfolders == value)
                {
                    return;
                }

                _includeSubfolders = value;
                RaisePropertyChanged(() => IncludeSubfolders);
                GetFiles();
            }
        }

        public string Info
        {
            get
            {
                var count = Files != null ? Files.Count : 0;
                return string.Format("Total: {0}", count);
            }
        }

        public string ViewTitle { get; private set; }

        #endregion //Properties

        #region BrowseCommand

        private ICommand _browseCommand;

        public ICommand BrowseCommand
        {
            get
            {
                return _browseCommand ??
                    (_browseCommand = new RelayCommand(BrowseCommandImpl));
            }
        }

        private void BrowseCommandImpl()
        {
            var fbd = new FolderBrowserDialog();
            fbd.Description = "Choose folder to scan";
            fbd.ShowNewFolderButton = false;
            if (fbd.ShowDialog() == DialogResult.OK)
                Path = fbd.SelectedPath;
        }

        #endregion BrowseCommand

        #region SaveCommand

        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                    (_saveCommand = new RelayCommand(SaveCommandImpl, CanSaveCommand));
            }
        }

        private bool CanSaveCommand()
        {
            return Path != null && Path.Length > 0;
        }

        private void SaveCommandImpl()
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            sfd.FilterIndex = 1;
            sfd.CheckPathExists = true;
            sfd.OverwritePrompt = true;
            sfd.Title = "Choose file to save result";
            sfd.InitialDirectory = Path;
            sfd.FileName = DateTime.Now.ToString("yyyy.MM.dd") + "_files.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                _fileInfoService.SaveFileInfo(sfd.FileName, Files, (ex) => { });
            }
        }

        #endregion SaveCommand

        private void GetFiles()
        {
            _fileInfoService.GetFileInfo(Path, string.IsNullOrEmpty(SearchPattern) ? DefaultSearchPattern : SearchPattern, IncludeSubfolders, (files, ex) =>
                {
                    if (ex == null)
                    {
                        Files = new ObservableCollection<string>(files);
                    }
                });
        }
    }
}