﻿using System;
using System.Collections.Generic;

namespace IndexMaker.Model
{
    public interface IFileInfoService
    {
        void SaveFileInfo(string filename, IEnumerable<string> fileInfos, Action<Exception> callback);

        void GetFileInfo(string path, string searchPattern, bool includeSubfolders, Action<IEnumerable<string>, Exception> callback);
    }
}