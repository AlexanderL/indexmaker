﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IndexMaker.Model
{
    public class FileInfoService : IFileInfoService
    {
        public void SaveFileInfo(string filename, IEnumerable<string> fileInfos, Action<Exception> callback)
        {
            var sb = new StringBuilder();
            foreach (var fileInfo in fileInfos)
            {
                sb.AppendLine(fileInfo);
            }
            try
            {
                using (StreamWriter outfile = new StreamWriter(filename))
                {
                    outfile.Write(sb.ToString());
                }
            }
            catch (IOException ex)
            {
                callback(ex);
            }
            catch(Exception ex)
            {
                callback(ex);
            }

            callback(null);
        }

        public void GetFileInfo(string path, string searchPattern, bool includeSubfolders, Action<IEnumerable<string>, Exception> callback)
        {
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    var files = Directory.EnumerateFiles(
                                            path, 
                                            searchPattern,
                                            includeSubfolders ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                                            .Select(p => Path.GetFileName(p));
                    callback(files, null);
                }
                catch (UnauthorizedAccessException ex)
                {
                    callback(null, ex);
                }
                catch (PathTooLongException ex)
                {
                    callback(null, ex);
                }
                catch(DirectoryNotFoundException ex)
                {
                    callback(null, ex);
                }
                catch(Exception ex)
                {
                    callback(null, ex);
                }
            }
            else
                callback(null, null);
        }
    }
}